# ChangeLog

## 0.1.2

## Development changes

- Add package `exec`
- Add function `exec.CheckProgram`

## 0.1.1

## Development changes

- Clanup dependencies

## 0.1.0

### User changes

First working version.

Initial packages:

- `print`
- `config`
