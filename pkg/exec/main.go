package exec

import (
	"os/exec"

	. "github.com/logrusorgru/aurora"

	"gitlab.com/fizzycfg/chips/airfrier/pkg/print"
)

// CheckProgram looks up if a program exists
func CheckProgram(name string) error {
	_, err := exec.LookPath(name)
	if err != nil {
		print.Fatal("program", Blue(name), "is not available")
	}
	return err
}
