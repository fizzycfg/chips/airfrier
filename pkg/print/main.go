package print

import (
	"fmt"
	"os"

	. "github.com/logrusorgru/aurora"
)

// Success prints a success message
func Success(a ...interface{}) {
	printPrefixed(BgGreen(Black(" ✓ ")), a...)
}

// Warning prints a warning message
func Warning(a ...interface{}) {
	printPrefixed(BgYellow(Black(" ! ")), a...)
}

// Error prints an error message
func Error(a ...interface{}) {
	printPrefixed(BgRed(Black(" ! ")), a...)
}

// Fatal prints an error message and exits
func Fatal(a ...interface{}) {
	Error(a...)
	os.Exit(1)
}

// printPrefixed is a utility function that prints a message with a specific prefix
func printPrefixed(prefix interface{}, a ...interface{}) {
	args := []interface{}{prefix}
	args = append(args, a...)
	fmt.Println(args...)
}
