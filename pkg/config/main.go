package config

import (
	"fmt"

	. "github.com/logrusorgru/aurora"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	print "gitlab.com/fizzycfg/chips/airfrier/pkg/print"
)

var cfgFile string

// Init reads in config file and ENV variables if set
func Init(programName string) {
	if cfgFile != "" {
		// Use config file from the flag
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory
		home, err := homedir.Dir()
		if err != nil {
			print.Fatal(err)
		}

		// Search config in home directory with same name as "programName"
		viper.AddConfigPath(home)
		viper.SetConfigName(fmt.Sprintf(".%s", programName))
	}

	// Read in environment variables that match
	viper.AutomaticEnv()

	// If a config file is found, read it in
	err := viper.ReadInConfig()
	if err == nil {
		print.Success("using config file", Blue(viper.ConfigFileUsed()))
	} else {
		print.Warning("skipping configuration:", err)
	}
}

// AddFlags adds flags related to configuration into the provided "command" argument
func AddFlags(command *cobra.Command) {
	command.PersistentFlags().StringVar(&cfgFile, "config", "", fmt.Sprintf("config file (default is $HOME/.%s.yaml)", command.Name()))

	if versionFlag := command.PersistentFlags().Lookup("verbose"); versionFlag != nil {
		viper.BindPFlag("verbose", versionFlag)
	}
}
